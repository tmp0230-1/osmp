
package com.osmp.web.test;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Description:JUNIT测试基类
 * 
 * @author: wangkaiping
 * @date: 2014年8月26日 上午9:40:29
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/spring-*.xml" })
public class BaseJunit4Test {

}
