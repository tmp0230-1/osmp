package com.osmp.web.system.cache.controller;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.osmp.web.system.cache.entity.CacheDefined;
import com.osmp.web.system.cache.entity.CacheInfo;
import com.osmp.web.system.cache.service.CacheService;
import com.osmp.web.system.servers.entity.Servers;
import com.osmp.web.system.servers.service.ServersService;

@Controller
@RequestMapping("cache")
public class CacheController {

    @Autowired
    private CacheService cacheService;
    
    @Autowired
	private ServersService serversService;

    @RequestMapping("/cacheOption")
    @ResponseBody
    public Map<String, Object> openCache(@RequestParam(value = "type", required = true) String type, String serverIp) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if ("close".equals(type)) {
                cacheService.close(serverIp);
            } else if ("open".equals(type)) {
                cacheService.open(serverIp);
            }
            map.put("success", true);
            map.put("msg", "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("success", false);
            map.put("msg", "删除失败");
        }

        return map;
    }

    @RequestMapping("/toList")
    public ModelAndView toList() {
//    	String res = cacheService.getCacheInfo();
//        return new ModelAndView("system/cache/cachelist", "cacheInfo", JSON.parseObject(res,
//                CacheInfo.class));
        
        ModelAndView mav = new ModelAndView("system/cache/cachelist");
		List<Servers> list = serversService.getAllServers("");
		mav.addObject("serverList", list);
		return mav;
    }
    
    @RequestMapping("/toCacheOfServer")
    public ModelAndView toCacheOfServer(@RequestParam("serverIp") String serverIp, HttpServletResponse response, PrintWriter out) {
    	String res = cacheService.getCacheInfo(serverIp);
    	
    	CacheInfo cacheInfo = new CacheInfo();
		try {
			cacheInfo = JSON.parseObject(res,CacheInfo.class);
		} catch (Exception e) {
			e.printStackTrace();
			response.setCharacterEncoding("utf-8");
			response.setHeader("Content-Type", "text/html;charset=utf-8");
			out.print("<script>alert('获取缓存列表失败，请检查osmp-config组件是否正常启动');history.back(1)</script>");
			out.flush();
			return null;
		}
    	
    	ModelAndView mav = new ModelAndView("system/cache/cachelistOfServer");
		mav.addObject("serverIp", serverIp);
		mav.addObject("cacheInfo", cacheInfo);
		return mav;
        
    }


    @RequestMapping("/cacheList")
    @ResponseBody
    public String cacheList(@RequestParam("serverIp") String serverIp) {
        String cacheDefinedLst = cacheService.getCacheList(serverIp);
        return cacheDefinedLst;
    }

    @RequestMapping("/toItem")
    public ModelAndView toItem(@RequestParam(value = "id", required = false) String cacheId, String serverIp) {
        Map<String, String> modal = new HashMap<String, String>(2);
        modal.put("cacheId", cacheId);
        modal.put("divName", "cacheDetail" + UUID.randomUUID().toString());
        modal.put("serverIp", serverIp);
        return new ModelAndView("system/cache/cachedetail", "modal", modal);
    }

    @RequestMapping("/itemList")
    @ResponseBody
    public String itemList(@RequestParam(value = "id", required = false) String cacheId, String serverIp) {
        Map<String, String> map = new HashMap<String, String>();
        if (!StringUtils.isBlank(cacheId)) {
            map.put("keys", cacheId);
        }
        return cacheService.getCacheItem(map, serverIp);
    }

    @RequestMapping("/toEdit")
    public ModelAndView toEdit(@RequestParam(value = "id") String cacheId,
            @RequestParam(value = "timeToLive") Integer timeToLive,
            @RequestParam(value = "timeToIdle") Integer timeToIdle, @RequestParam(value = "state") Integer state) {
        CacheDefined cd = new CacheDefined();
        cd.setId(cacheId);
        cd.setState(state);
        cd.setTimeToLive(timeToLive);
        cd.setTimeToIdle(timeToIdle);

        return new ModelAndView("system/cache/cacheEdit", "cacheDefined", cd);
    }

    @RequestMapping("/deleteCache")
    @ResponseBody
    public Map<String, Object> deleteCache(
            @RequestParam(value = "id", required = false, defaultValue = "-1") String itemKey, String serverIp) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            cacheService.deleteCache(itemKey, serverIp);
            map.put("success", true);
            map.put("msg", "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("success", false);
            map.put("msg", "删除失败");
        }

        return map;
    }

    @RequestMapping("/updateCache")
    @ResponseBody
    public Map<String, Object> updateCache(CacheDefined cacheDefined, String serverIp) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            cacheService.updateCache(cacheDefined, serverIp);
            map.put("success", true);
            map.put("msg", "更新成功");
        } catch (Exception e) {
            e.printStackTrace();
            map.put("success", false);
            map.put("msg", "更新失败");
        }

        return map;
    }

}
