 /*   
 * Project: OSMP
 * FileName: Request.java
 * version: V1.0
 */
package com.osmp.intf.define.server;

import java.io.Serializable;
import java.util.Map;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2017年2月7日 上午11:57:11上午10:51:30
 */
public class Request implements Serializable {

	private static final long serialVersionUID = -988938028226863922L;
	
	private final String msgId;
	
	private final Map<String,String> source;
	
	private final String service;
	
	private final Object parameter;

	public Request(String msgId, Map<String, String> source, String service, Object parameter) {
		super();
		this.msgId = msgId;
		this.source = source;
		this.service = service;
		this.parameter = parameter;
	}

	/**
	 * @return the msgId
	 */
	public String getMsgId() {
		return msgId;
	}

	/**
	 * @return the source
	 */
	public Map<String, String> getSource() {
		return source;
	}

	/**
	 * @return the service
	 */
	public String getService() {
		return service;
	}

	/**
	 * @return the parameter
	 */
	public Object getParameter() {
		return parameter;
	}

	@Override
	public String toString() {
		return "msgId="+msgId+", service="+service+", source="+source.toString()+", parameter=" + parameter.toString();
	}
	
	
	

}
